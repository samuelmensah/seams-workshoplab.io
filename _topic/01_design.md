---
slug: design
title: Project Design & Planning
---
SEAMS is about

 - **S***oftware*: useful computer abstractions for transforming inputs into outputs, created by...
 - **E***ngineering*: an organized, systematic approach to design and delivery of reliable, flexible, practical, and human-useable systems for...
 - **A***pplied*: considering empirical data...
 - **M***athematical*: within quantitative, rules-based representations...
 - **S***ciences*: that make testable predictions.

These interpretations can guide the way you approach a new research problem--or, put another way, they can help you with *design* and *planning* for the work that will solve that research problem.

<!--
We select participants already know some basics relative to these topics.  You should already know how to "code" -- *e.g.* how to declare variables and functions, read and write files, and generally arrange that syntax in a way that does useful things -- which are the bricks and mortar of Software Engineering (or wires and resistors, whatever your preferred analogy assembles into products).  Similarly, you should already know a bit about research -- *i.e.*, gathering data, formulating and testing hypotheses 

* * *



## Research

For this workshop, how do we define research?

## The Lab Notebook

Scientists do not leave critical details to fallible memory: we record our observations, and our method to obtain them, in detail.  The reasons to do so might seem obvious, but are worth actively contemplating.  Science is not the pursuit of *my* truth or *your* truth or a *Western* truth or an *African* truth.  We are imperfect creatures in this pursuit, but our standard is *general, objective truth*, not subjective or socially constructed.  The simplest is that our work relies on trust, trust is built by openness, and openness means have an accessible record.

The goal of scientific practice is to expand the usefully understood.  If some phenomena was not previously understood, then it is unlikely to be extremely obvious.

 - *Accountability*:  
 - *Explanations*

## Experimental Apparatus

## References

[*Your Research Project: Designing and Planning Your Work* by Nicholas Walliman](https://www.nyu.edu/classes/bkg/methods/010072.pdf)

Some references on software testing:

 - [An introduction to software testing](http://agile.csc.ncsu.edu/SEMaterials/BlackBox.pdf)
 - [A general guide to testing in Python](http://docs.python-guide.org/en/latest/writing/tests/)
 - [A guide to `unittest` (PyUnit)](http://www.drdobbs.com/testing/unit-testing-with-python/240165163)
 - [...and another](http://pythontesting.net/framework/unittest/unittest-introduction/)
 - Some tricks with PyCharm:
   * [running and debugging](https://www.jetbrains.com/pycharm/help/running-and-debugging.html)
   * [python debugger](https://www.jetbrains.com/pycharm/help/python-debugger.html)
   * [pycharm debug console](https://www.jetbrains.com/pycharm/help/using-debug-console.html)
   * [stepping through program](https://www.jetbrains.com/pycharm/help/stepping-through-the-program.html)
 - [Some tricks with Rstudio](https://support.rstudio.com/hc/en-us/articles/205612627-Debugging-with-RStudio)
 - [using R package testthat](http://journal.r-project.org/archive/2011-1/RJournal_2011-1_Wickham.pdf)
 - [thoughts on input validation](http://www.ibm.com/developerworks/library/l-sp2/)
 - [Tests for randomness](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.156.7149&rep=rep1&type=pdf)
 - [Thinking a bit about what testing means.](http://www.nytimes.com/interactive/2015/07/03/upshot/a-quick-puzzle-to-test-your-problem-solving.html)

-->